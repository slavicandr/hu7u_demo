HEADS UP 7 UP Demo site upgrade
Author: Shea McKinney (shea@imagexmedia.com) twitter/D.org: sherakama
March 24th 2012
------------------------------------------------------------------------




REQUIREMENTS:
------------------------------------------------------------------------

Git
Drush (5 is best)
PHP 5.2+
mysql 4+
mysqldump




ACCOUNT INFORMATION:
------------------------------------------------------------------------

Drupal Admin user
username:	admin
password:	admin





GIT BRANCHES:
------------------------------------------------------------------------

In this repository there are 4 branches

1. master (the current branch you are on)
The master branch just contains these instruction files

2. d6
The starting point for this demo. Contains a Drupal 6 core, contrib modules, the upgrade scripts, and database dumps for both the completely set up version of the d6 site and the ready for upgrade version with no contrib modules enabled

3. d7-nocontrib
This is the second stage of the upgrade script. This repository contains the drupal 7 core files without any contributed modules. This branch is used to run the core updates

4. d7
This final branch contains the full D7 site files and a Database dump of the end result site.





SETUP AND EXECUTION:
-------------------------------------------------------------------------

• Create a database for this project
• Checkout the d6 branch
• Import the db6.sql dump file into your newly created database. The db6.sql file in the Drupal root folder. The db6-core.sql file is the D6 site with staged with all of the contributed modules disabled.
• Connect the site to the database by creating a sites/default/settings.php and adding in your database connection information
• Check to see that the site loads the D6 version and that you can log in using user "admin" with password "admin".
• Check the update status report and look for any outdated modules. For a real life upgrade you should update these modules to the latest development version. This may break the demonstration however.
• Allow write access to default and settings.php as the upgrade script will write to the settings.php file
• Open up your favorite command line interface and navigate to the root folder of the d6 installation
• Run upgrade/runall.sh from the root of the site with 'sh upgrade/runall.sh'. This will run through all the necessary upgrade steps. Check out upgrade/runall.sh for comments on what is happening.
• Once upgrade has completed log into the site and navigate to /admin/structure/content_migrate in order to run the upgrade field process on the missing cck fields.





TROUBLESHOOT:
--------------------------------------------------------------------------

Drush and the command line need access to a few items:

1. PHP
2. mysql and mysqldump

If you are running MAMP or something similar please look at http://forum.mamp.info/viewtopic.php?f=2&t=7545
for information on how to configure your environment.

